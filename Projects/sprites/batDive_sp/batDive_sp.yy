{
    "id": "6553cf5f-3a06-4324-98b1-2b91bfc9d131",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batDive_sp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72d6d64e-da0e-4d9b-8f2e-92f0e0ce7856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553cf5f-3a06-4324-98b1-2b91bfc9d131",
            "compositeImage": {
                "id": "de91d7fe-510a-46fc-9e86-2970eb65db29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72d6d64e-da0e-4d9b-8f2e-92f0e0ce7856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e856bb45-5c38-4eb0-a763-ffdb58211323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72d6d64e-da0e-4d9b-8f2e-92f0e0ce7856",
                    "LayerId": "c71cbdf3-b97b-4383-9bc3-9bcee283228e"
                }
            ]
        },
        {
            "id": "dc69f0f2-7abb-44f7-bb52-1f3327ebb1fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553cf5f-3a06-4324-98b1-2b91bfc9d131",
            "compositeImage": {
                "id": "73f3b57d-9658-4bf8-9744-5c360f079d22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc69f0f2-7abb-44f7-bb52-1f3327ebb1fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f68584b4-e6e8-46f3-a9d4-2b1098cddeac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc69f0f2-7abb-44f7-bb52-1f3327ebb1fd",
                    "LayerId": "c71cbdf3-b97b-4383-9bc3-9bcee283228e"
                }
            ]
        },
        {
            "id": "54a1affd-8f17-4a67-b9a5-28c56c947fb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553cf5f-3a06-4324-98b1-2b91bfc9d131",
            "compositeImage": {
                "id": "07c9dcd8-e4a2-4ba4-a157-a2b2fe416595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54a1affd-8f17-4a67-b9a5-28c56c947fb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051e4392-c3c1-4d40-ac62-de6bcdcbed48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54a1affd-8f17-4a67-b9a5-28c56c947fb1",
                    "LayerId": "c71cbdf3-b97b-4383-9bc3-9bcee283228e"
                }
            ]
        },
        {
            "id": "30a4a82e-6945-49b9-8c59-24860a13ec95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553cf5f-3a06-4324-98b1-2b91bfc9d131",
            "compositeImage": {
                "id": "7499b9d0-77af-49fe-b512-c6d8694df906",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30a4a82e-6945-49b9-8c59-24860a13ec95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd0a0ed9-6c24-423f-b974-ba2f523cc44d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30a4a82e-6945-49b9-8c59-24860a13ec95",
                    "LayerId": "c71cbdf3-b97b-4383-9bc3-9bcee283228e"
                }
            ]
        },
        {
            "id": "a7bd7fa8-8dfd-4259-8290-e7621d67d442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553cf5f-3a06-4324-98b1-2b91bfc9d131",
            "compositeImage": {
                "id": "ab4c149f-4603-4796-90d5-584e86733b0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7bd7fa8-8dfd-4259-8290-e7621d67d442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60254ff4-c410-4b3b-b339-d070fe20b570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7bd7fa8-8dfd-4259-8290-e7621d67d442",
                    "LayerId": "c71cbdf3-b97b-4383-9bc3-9bcee283228e"
                }
            ]
        },
        {
            "id": "64c60a3a-c342-481c-aadb-ef0e36bcacae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553cf5f-3a06-4324-98b1-2b91bfc9d131",
            "compositeImage": {
                "id": "6c682ce6-9ab2-46b1-99fa-3ab881b29261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64c60a3a-c342-481c-aadb-ef0e36bcacae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82b828c0-9dd4-4537-b287-df83bccc5d8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64c60a3a-c342-481c-aadb-ef0e36bcacae",
                    "LayerId": "c71cbdf3-b97b-4383-9bc3-9bcee283228e"
                }
            ]
        },
        {
            "id": "0044bdd9-0ce7-4c66-a974-20f050f87b44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553cf5f-3a06-4324-98b1-2b91bfc9d131",
            "compositeImage": {
                "id": "83093ac9-95bd-4e8d-a46b-f67bcb367638",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0044bdd9-0ce7-4c66-a974-20f050f87b44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48c4baf2-abc1-43a3-ae52-e6fcaaad0b40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0044bdd9-0ce7-4c66-a974-20f050f87b44",
                    "LayerId": "c71cbdf3-b97b-4383-9bc3-9bcee283228e"
                }
            ]
        },
        {
            "id": "658ba765-9a20-4276-987f-5117dbd3586e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553cf5f-3a06-4324-98b1-2b91bfc9d131",
            "compositeImage": {
                "id": "1e705c05-ea29-43d9-8341-52eafae5679a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "658ba765-9a20-4276-987f-5117dbd3586e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ee1bc10-1cae-4596-82f6-af7dfd0cd84a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658ba765-9a20-4276-987f-5117dbd3586e",
                    "LayerId": "c71cbdf3-b97b-4383-9bc3-9bcee283228e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c71cbdf3-b97b-4383-9bc3-9bcee283228e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6553cf5f-3a06-4324-98b1-2b91bfc9d131",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}