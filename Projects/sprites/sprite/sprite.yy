{
    "id": "0cc84695-c036-4236-8377-9ea0336aae7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c51957c0-4fd9-472e-9eb3-d9d086088ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cc84695-c036-4236-8377-9ea0336aae7d",
            "compositeImage": {
                "id": "718ae115-01c1-4833-81b7-3ac4c254eebf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c51957c0-4fd9-472e-9eb3-d9d086088ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33e39c24-65ac-4ded-9d5b-c0beeb6d353a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c51957c0-4fd9-472e-9eb3-d9d086088ab5",
                    "LayerId": "4a197026-93d2-4982-9a73-0f525b6ca70d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4a197026-93d2-4982-9a73-0f525b6ca70d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cc84695-c036-4236-8377-9ea0336aae7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}