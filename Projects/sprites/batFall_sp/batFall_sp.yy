{
    "id": "0677359e-9f6c-4b3c-947d-527d2d13ac1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batFall_sp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f61cfd9-c142-4799-bb7f-9cc8ec6255dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0677359e-9f6c-4b3c-947d-527d2d13ac1d",
            "compositeImage": {
                "id": "3341faaf-fa22-41fb-a275-16181cd81fb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f61cfd9-c142-4799-bb7f-9cc8ec6255dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d3f5d12-b100-4f12-86aa-f7621c68c528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f61cfd9-c142-4799-bb7f-9cc8ec6255dc",
                    "LayerId": "4583ddae-c256-4854-a5c8-11099cb574a6"
                }
            ]
        },
        {
            "id": "530321db-a789-4f16-b1d9-37441fefba23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0677359e-9f6c-4b3c-947d-527d2d13ac1d",
            "compositeImage": {
                "id": "ced6f434-bad7-4d95-b465-3a5ff8d0c585",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "530321db-a789-4f16-b1d9-37441fefba23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d93bbef1-0864-44b2-97a1-a9d19e122c84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "530321db-a789-4f16-b1d9-37441fefba23",
                    "LayerId": "4583ddae-c256-4854-a5c8-11099cb574a6"
                }
            ]
        },
        {
            "id": "f6ef5617-fd31-418f-8352-5a2f767307ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0677359e-9f6c-4b3c-947d-527d2d13ac1d",
            "compositeImage": {
                "id": "17d01223-8075-44c4-a9a4-110b3b1fdd71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6ef5617-fd31-418f-8352-5a2f767307ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeb7ce97-8049-49da-88a3-90468103d87a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6ef5617-fd31-418f-8352-5a2f767307ae",
                    "LayerId": "4583ddae-c256-4854-a5c8-11099cb574a6"
                }
            ]
        },
        {
            "id": "246e50a2-94a8-4d66-af8d-54e8f15fa41e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0677359e-9f6c-4b3c-947d-527d2d13ac1d",
            "compositeImage": {
                "id": "08d518b0-c72e-4a63-8c5a-d3d198f4e412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "246e50a2-94a8-4d66-af8d-54e8f15fa41e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db7cbc7f-a82c-4e8e-a7e2-1680317c2a94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "246e50a2-94a8-4d66-af8d-54e8f15fa41e",
                    "LayerId": "4583ddae-c256-4854-a5c8-11099cb574a6"
                }
            ]
        },
        {
            "id": "5014d480-6438-48e2-9e40-5a2092eda7a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0677359e-9f6c-4b3c-947d-527d2d13ac1d",
            "compositeImage": {
                "id": "06d88a05-5fef-4137-9f18-6947041cea97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5014d480-6438-48e2-9e40-5a2092eda7a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a05e6f2-a1bc-43bc-9563-c93427864c95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5014d480-6438-48e2-9e40-5a2092eda7a0",
                    "LayerId": "4583ddae-c256-4854-a5c8-11099cb574a6"
                }
            ]
        },
        {
            "id": "d3e3438b-47c3-471d-b6a1-03b345706a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0677359e-9f6c-4b3c-947d-527d2d13ac1d",
            "compositeImage": {
                "id": "b5990a45-cac4-48cd-8062-3647a53ef50c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3e3438b-47c3-471d-b6a1-03b345706a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75ee5c91-47ee-42e5-b2e0-1838039fa43a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3e3438b-47c3-471d-b6a1-03b345706a05",
                    "LayerId": "4583ddae-c256-4854-a5c8-11099cb574a6"
                }
            ]
        },
        {
            "id": "9df0a113-3fea-4b6c-8330-1febf990139a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0677359e-9f6c-4b3c-947d-527d2d13ac1d",
            "compositeImage": {
                "id": "241b5a5a-da8d-4d3a-aeca-d73ff34260cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9df0a113-3fea-4b6c-8330-1febf990139a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e057318-0940-48b2-8702-61e93d509ac7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9df0a113-3fea-4b6c-8330-1febf990139a",
                    "LayerId": "4583ddae-c256-4854-a5c8-11099cb574a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4583ddae-c256-4854-a5c8-11099cb574a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0677359e-9f6c-4b3c-947d-527d2d13ac1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0.25,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}