{
    "id": "f6787f79-4aca-469b-8120-506be78fd1e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cave2_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "510b5aeb-e1ba-41cd-b4cb-47dbcdafa9ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6787f79-4aca-469b-8120-506be78fd1e0",
            "compositeImage": {
                "id": "f0a37a38-ef5c-4fd2-a430-131ce7a7e53e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "510b5aeb-e1ba-41cd-b4cb-47dbcdafa9ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b3cb688-135e-4ddf-a6bf-8bf594e20feb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "510b5aeb-e1ba-41cd-b4cb-47dbcdafa9ca",
                    "LayerId": "5696c76d-1435-4ad4-9472-66effd6e5090"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "5696c76d-1435-4ad4-9472-66effd6e5090",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6787f79-4aca-469b-8120-506be78fd1e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}