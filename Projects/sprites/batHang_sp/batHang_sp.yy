{
    "id": "65d5daff-9f04-4a3e-9804-d2bfb2c72cc3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batHang_sp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7523b80-7ec5-4466-ae20-7359964a8e0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d5daff-9f04-4a3e-9804-d2bfb2c72cc3",
            "compositeImage": {
                "id": "cf351a16-33f9-4422-a39b-e0ed566e1bcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7523b80-7ec5-4466-ae20-7359964a8e0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "073426ef-10bf-4fda-a72f-7679faa69cf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7523b80-7ec5-4466-ae20-7359964a8e0f",
                    "LayerId": "6a5e7e1a-9e84-4900-b373-4c0a7f0d4a87"
                }
            ]
        },
        {
            "id": "c097defa-065e-4724-bbf4-e04b542e522f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d5daff-9f04-4a3e-9804-d2bfb2c72cc3",
            "compositeImage": {
                "id": "d67351f9-5257-49ec-b80b-dd036e38ad77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c097defa-065e-4724-bbf4-e04b542e522f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da8aeb6a-aa7b-4931-9892-d857f2bc5ffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c097defa-065e-4724-bbf4-e04b542e522f",
                    "LayerId": "6a5e7e1a-9e84-4900-b373-4c0a7f0d4a87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6a5e7e1a-9e84-4900-b373-4c0a7f0d4a87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65d5daff-9f04-4a3e-9804-d2bfb2c72cc3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0.25,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}