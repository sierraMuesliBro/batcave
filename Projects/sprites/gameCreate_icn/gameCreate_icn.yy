{
    "id": "168b4033-c28d-4b06-b189-17b21b54ad33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gameCreate_icn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97a3f899-96d3-426e-bc5f-ef0f9de19642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "168b4033-c28d-4b06-b189-17b21b54ad33",
            "compositeImage": {
                "id": "2ff08666-d30c-4acf-b57e-09e7d9ae3298",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97a3f899-96d3-426e-bc5f-ef0f9de19642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c492efb2-d111-41eb-8682-0beffca4c526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97a3f899-96d3-426e-bc5f-ef0f9de19642",
                    "LayerId": "43540a93-a5fb-4535-b044-40c5e1bee778"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "43540a93-a5fb-4535-b044-40c5e1bee778",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "168b4033-c28d-4b06-b189-17b21b54ad33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}