{
    "id": "f85048c4-9482-47ad-ba07-02fcf3373aed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batFlap_sp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9af9cd0c-a074-482c-a2c8-e4afdcd8a103",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f85048c4-9482-47ad-ba07-02fcf3373aed",
            "compositeImage": {
                "id": "057219c5-c659-4ae3-90ed-7b4b77442b31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9af9cd0c-a074-482c-a2c8-e4afdcd8a103",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "591c6ef3-f51b-469f-97eb-1a195112bcac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9af9cd0c-a074-482c-a2c8-e4afdcd8a103",
                    "LayerId": "ea264a4d-5236-43da-995b-5bb1438bb845"
                }
            ]
        },
        {
            "id": "49f7cfd4-c979-4085-958f-1183d5ce7acd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f85048c4-9482-47ad-ba07-02fcf3373aed",
            "compositeImage": {
                "id": "55f167ab-c199-496f-ab2d-336eabd48434",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49f7cfd4-c979-4085-958f-1183d5ce7acd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44d2f126-4fb9-43a4-9ba6-1ac622a11b69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49f7cfd4-c979-4085-958f-1183d5ce7acd",
                    "LayerId": "ea264a4d-5236-43da-995b-5bb1438bb845"
                }
            ]
        },
        {
            "id": "18e1a407-cca0-41fb-bca2-8b4df95c9d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f85048c4-9482-47ad-ba07-02fcf3373aed",
            "compositeImage": {
                "id": "9856db53-3c50-4e3f-bfb7-396aa4bc7024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18e1a407-cca0-41fb-bca2-8b4df95c9d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73852ab1-6cc6-41f2-8440-496e66109db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18e1a407-cca0-41fb-bca2-8b4df95c9d78",
                    "LayerId": "ea264a4d-5236-43da-995b-5bb1438bb845"
                }
            ]
        },
        {
            "id": "5c393f01-8542-4621-94b0-828ae2dd1cf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f85048c4-9482-47ad-ba07-02fcf3373aed",
            "compositeImage": {
                "id": "166bc754-9323-4e32-87f3-012eba208ec9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c393f01-8542-4621-94b0-828ae2dd1cf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dcab9c3-d1de-4db4-9728-d3bb17503c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c393f01-8542-4621-94b0-828ae2dd1cf2",
                    "LayerId": "ea264a4d-5236-43da-995b-5bb1438bb845"
                }
            ]
        },
        {
            "id": "e2918c27-71f3-4f4a-ab70-a587dfac7c55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f85048c4-9482-47ad-ba07-02fcf3373aed",
            "compositeImage": {
                "id": "20c0b578-a362-44c4-a403-d35b2efa2b51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2918c27-71f3-4f4a-ab70-a587dfac7c55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b606e40-ac4e-4969-a3fc-2fd5ba63d72f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2918c27-71f3-4f4a-ab70-a587dfac7c55",
                    "LayerId": "ea264a4d-5236-43da-995b-5bb1438bb845"
                }
            ]
        },
        {
            "id": "e163c794-dff5-4111-913e-95eab94abea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f85048c4-9482-47ad-ba07-02fcf3373aed",
            "compositeImage": {
                "id": "042c66db-f053-4fd2-8ab0-49a1ad13b4bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e163c794-dff5-4111-913e-95eab94abea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c3897e-3378-48fc-b195-362bf0ad10c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e163c794-dff5-4111-913e-95eab94abea8",
                    "LayerId": "ea264a4d-5236-43da-995b-5bb1438bb845"
                }
            ]
        },
        {
            "id": "98d2ab4c-ca2f-4a06-b6b7-e2a4feb1a8d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f85048c4-9482-47ad-ba07-02fcf3373aed",
            "compositeImage": {
                "id": "9adffb95-8ff8-413d-b5dc-2fd3d978a88c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98d2ab4c-ca2f-4a06-b6b7-e2a4feb1a8d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a95abc5-e829-41a0-89bc-ccd991c4e053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d2ab4c-ca2f-4a06-b6b7-e2a4feb1a8d2",
                    "LayerId": "ea264a4d-5236-43da-995b-5bb1438bb845"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ea264a4d-5236-43da-995b-5bb1438bb845",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f85048c4-9482-47ad-ba07-02fcf3373aed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}