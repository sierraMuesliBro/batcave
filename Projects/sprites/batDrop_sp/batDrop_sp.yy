{
    "id": "94fdfd97-a887-4635-bced-2d5aebac53b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batDrop_sp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f41a3ea-7818-4fc8-99f2-115f1b532a59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94fdfd97-a887-4635-bced-2d5aebac53b2",
            "compositeImage": {
                "id": "b0555849-9761-4b47-b66e-3c82efbafc65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f41a3ea-7818-4fc8-99f2-115f1b532a59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36a55d56-5aa5-473f-958c-a7dc030a792d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f41a3ea-7818-4fc8-99f2-115f1b532a59",
                    "LayerId": "61a635d5-6ee4-466e-8192-a7dfdb458ebd"
                }
            ]
        },
        {
            "id": "d7b7fa91-2598-4fae-b253-a579fd3cde12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94fdfd97-a887-4635-bced-2d5aebac53b2",
            "compositeImage": {
                "id": "f5263fcc-3cd3-4e46-8712-3fd706dc07d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7b7fa91-2598-4fae-b253-a579fd3cde12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccf93caf-81bb-42c0-adaf-527c56704a81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7b7fa91-2598-4fae-b253-a579fd3cde12",
                    "LayerId": "61a635d5-6ee4-466e-8192-a7dfdb458ebd"
                }
            ]
        },
        {
            "id": "e3a93a05-32bc-4e36-a112-1011cdba9045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94fdfd97-a887-4635-bced-2d5aebac53b2",
            "compositeImage": {
                "id": "752428bd-75d6-4832-9982-9e8d53aaba31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3a93a05-32bc-4e36-a112-1011cdba9045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f044d0ac-4693-424d-8cf1-da5c4d823619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3a93a05-32bc-4e36-a112-1011cdba9045",
                    "LayerId": "61a635d5-6ee4-466e-8192-a7dfdb458ebd"
                }
            ]
        },
        {
            "id": "78fdef9c-191a-4cdc-94e7-96c5099c778d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94fdfd97-a887-4635-bced-2d5aebac53b2",
            "compositeImage": {
                "id": "e2e7ee73-210c-4b76-bf60-54dfe480aad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78fdef9c-191a-4cdc-94e7-96c5099c778d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c91f9ed-0529-4a71-bf0c-7e2d842e2f15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78fdef9c-191a-4cdc-94e7-96c5099c778d",
                    "LayerId": "61a635d5-6ee4-466e-8192-a7dfdb458ebd"
                }
            ]
        },
        {
            "id": "4b4d718a-72d3-44b3-8b8c-bd2c2aef3ad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94fdfd97-a887-4635-bced-2d5aebac53b2",
            "compositeImage": {
                "id": "580d7365-0bb2-4fcb-b4c0-3eeb7379793e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b4d718a-72d3-44b3-8b8c-bd2c2aef3ad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e86ad68-cebf-4a25-af88-927e03a2ae86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b4d718a-72d3-44b3-8b8c-bd2c2aef3ad8",
                    "LayerId": "61a635d5-6ee4-466e-8192-a7dfdb458ebd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "61a635d5-6ee4-466e-8192-a7dfdb458ebd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94fdfd97-a887-4635-bced-2d5aebac53b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0.25,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}