{
    "id": "c19fd422-e7c1-40a9-b37b-ef58a7bdde92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cave_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a76e812-0b55-4945-8465-975547df4fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c19fd422-e7c1-40a9-b37b-ef58a7bdde92",
            "compositeImage": {
                "id": "124f2955-bf47-4693-9c65-cfc194a2a61d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a76e812-0b55-4945-8465-975547df4fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5057aa5-455c-46e5-a41f-4852ffaca2f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a76e812-0b55-4945-8465-975547df4fde",
                    "LayerId": "32b60d09-7930-4d72-bc29-ee6488a67184"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "32b60d09-7930-4d72-bc29-ee6488a67184",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c19fd422-e7c1-40a9-b37b-ef58a7bdde92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}