{
    "id": "f5d7a388-f29b-466f-a2b2-f5d1e5fa488b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "caveTileset_ts",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 32,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d34d24f-30ac-43dc-a6f8-6fe0be680532",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5d7a388-f29b-466f-a2b2-f5d1e5fa488b",
            "compositeImage": {
                "id": "a29e083d-490e-4724-b94e-5ebc55a31735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d34d24f-30ac-43dc-a6f8-6fe0be680532",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa52b230-b534-4fd8-9546-40fc9312af6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d34d24f-30ac-43dc-a6f8-6fe0be680532",
                    "LayerId": "d84db853-fef1-4e01-a1bb-6da11df9d67b"
                },
                {
                    "id": "25d08ada-c3dc-43a4-b907-07b135588ec5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d34d24f-30ac-43dc-a6f8-6fe0be680532",
                    "LayerId": "66b4510a-21f9-42bc-8bd9-eb9a915d81fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "66b4510a-21f9-42bc-8bd9-eb9a915d81fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5d7a388-f29b-466f-a2b2-f5d1e5fa488b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "d84db853-fef1-4e01-a1bb-6da11df9d67b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5d7a388-f29b-466f-a2b2-f5d1e5fa488b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}