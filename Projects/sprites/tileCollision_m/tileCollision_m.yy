{
    "id": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tileCollision_m",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6a25e2f-c71d-49a9-8e9d-b98b8b63fc0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
            "compositeImage": {
                "id": "8bd8f047-5d27-4417-80cf-f1ec07b5d980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6a25e2f-c71d-49a9-8e9d-b98b8b63fc0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb252572-ffb6-4d35-9104-478625be9a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6a25e2f-c71d-49a9-8e9d-b98b8b63fc0d",
                    "LayerId": "478b4347-9330-4d67-a1e7-557daf27df67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "478b4347-9330-4d67-a1e7-557daf27df67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}