{
    "id": "4a5324e7-ed24-4ace-87d8-513130c0bf84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7490969-e69b-4860-b7bb-2a3a8c3aacbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5324e7-ed24-4ace-87d8-513130c0bf84",
            "compositeImage": {
                "id": "2a6fd565-053c-4d15-b63f-2ab303017016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7490969-e69b-4860-b7bb-2a3a8c3aacbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84528459-0649-4c89-a92f-021838ec3f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7490969-e69b-4860-b7bb-2a3a8c3aacbb",
                    "LayerId": "22039c26-a7ba-444f-9087-d18c5107ff2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "22039c26-a7ba-444f-9087-d18c5107ff2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a5324e7-ed24-4ace-87d8-513130c0bf84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}