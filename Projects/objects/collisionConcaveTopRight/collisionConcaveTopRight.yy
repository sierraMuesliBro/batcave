{
    "id": "320d060e-0d1e-408b-a852-5194c2240813",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionConcaveTopRight",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "4b372a6a-2b84-4ed3-86b7-c121d609b51d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 32
        },
        {
            "id": "73311678-150c-4145-8a62-a0c8427ddecc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "400b1dfe-1d42-4b62-8e65-1b449f154c6d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        },
        {
            "id": "99040558-6113-435f-a81a-42bd00b0b15b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "7b84f875-1e57-4f3a-aed4-2a61221e7e66",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}