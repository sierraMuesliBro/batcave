{
    "id": "c6d4668f-0f85-4e06-9adb-0fe0e648fd65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionFill",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.5,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "d6cd8fd3-e87d-4425-8d24-57478e42f96d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        },
        {
            "id": "ff591e96-9789-4050-a07f-09901e7a9b52",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 32
        },
        {
            "id": "ccde99a1-1f36-4454-b8a9-83d00b8154b5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "9168f998-fbe5-41d7-aff3-e8b2de5ebf9b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}