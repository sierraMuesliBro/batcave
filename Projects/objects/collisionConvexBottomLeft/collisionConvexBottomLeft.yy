{
    "id": "f2767a72-214c-4e0c-be93-e2a7a9b5137a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionConvexBottomLeft",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "ed6e9e8a-9f63-447d-9c28-04c38ebb02fe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "679b5da5-6126-42e4-af3d-f1cb1329025c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "827a4b23-0208-4b59-a23b-64bf6faccdbd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 32
        },
        {
            "id": "4ae9c959-0c0a-479a-a5f9-08eee2f1a2e2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 24
        },
        {
            "id": "525fa202-54b1-4b01-9e2a-70d4b3b66838",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 40,
            "y": 16
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}