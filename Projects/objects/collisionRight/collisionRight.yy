{
    "id": "86f204af-31a1-4b7c-b669-1eed050aaa9b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionRight",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "2d755bd8-6f67-4b25-9c25-54e30ca230d0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "2a203b4b-e98e-41de-93d7-50e0f55d3acb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "3951aeac-493d-4fa6-9009-205b9d402b01",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 64
        },
        {
            "id": "b58c4287-f0a5-446f-a21f-c63a48523987",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}