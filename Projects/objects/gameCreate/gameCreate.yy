{
    "id": "333a86ac-4c7e-4ca3-9093-1b14f2851e39",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "gameCreate",
    "eventList": [
        {
            "id": "8d5d991a-1f59-4e7b-bf50-f60dd45fb739",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "333a86ac-4c7e-4ca3-9093-1b14f2851e39"
        },
        {
            "id": "1aaa9535-2dd6-427b-b0aa-e35eb7f515bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "333a86ac-4c7e-4ca3-9093-1b14f2851e39"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "168b4033-c28d-4b06-b189-17b21b54ad33",
    "visible": true
}