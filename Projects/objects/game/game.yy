{
    "id": "6fb4cf32-9429-447f-a9f7-0db37ae845b9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "game",
    "eventList": [
        {
            "id": "a273af1b-c14c-4c88-a642-466d265fcfa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6fb4cf32-9429-447f-a9f7-0db37ae845b9"
        },
        {
            "id": "2e436c8d-14cc-4279-bd78-bb0464580671",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 9,
            "m_owner": "6fb4cf32-9429-447f-a9f7-0db37ae845b9"
        },
        {
            "id": "0a43a5be-caee-4033-af02-e5c96ce9fcf4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6fb4cf32-9429-447f-a9f7-0db37ae845b9"
        },
        {
            "id": "a2e569c5-9c9a-4785-a934-aca7d27ba2ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "6fb4cf32-9429-447f-a9f7-0db37ae845b9"
        },
        {
            "id": "5b1a88b4-d680-48b8-b1f4-a87f6f07548b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6fb4cf32-9429-447f-a9f7-0db37ae845b9"
        },
        {
            "id": "898c62d4-4563-4229-915d-529231b8e4f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "6fb4cf32-9429-447f-a9f7-0db37ae845b9"
        },
        {
            "id": "2059d4e0-7505-45dc-9c36-42b267a44b1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 8,
            "m_owner": "6fb4cf32-9429-447f-a9f7-0db37ae845b9"
        },
        {
            "id": "f72e94d4-f46d-48d6-b182-beabf5ed2a8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6fb4cf32-9429-447f-a9f7-0db37ae845b9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "168b4033-c28d-4b06-b189-17b21b54ad33",
    "visible": true
}