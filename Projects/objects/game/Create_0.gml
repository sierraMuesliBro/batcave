/// create
drawCreate = true;
alCreate = 0;
alarm[alCreate] = 1;

// view
viewIndex = 0;
viewSizeX = 512;
viewSizeY = 512;


// camera
cameraX = 0;
cameraY = 0;
cameraXPre = 0;
cameraYPre = 0;
cameraDirection = 0;

windowSizeX = 1024;
windowSizeY = 1024;

surfaceSizeX = viewSizeX;
surfaceSizeY = viewSizeY;
surface = surface_create(surfaceSizeX, surfaceSizeY);
application_surface_enable(false);

// view edge
viewEdgeColour = make_color_rgb(255, 255, 127);

viewEdgeSurface = noone;
viewEdgeSurfaceSizeX = viewSizeX+1;
viewEdgeSurfaceSizeY = viewSizeY+1;
viewEdgeSurfaceUpdate = true;
viewEdgeSurfaceSprite = noone;



// kill
killSurface = noone;
killSurfaceSizeX = viewSizeX/2;
killSurfaceSizeY = viewSizeY;
killSurfaceUpdate = true;
killSurfaceSprite = noone;

killDirection = 0;
killDirectionRange = 90;
killDirectionMax =  killDirectionRange;
killDirectionMin = -killDirectionRange;

