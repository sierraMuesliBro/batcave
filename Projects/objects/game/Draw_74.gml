/// @description Insert description here
if (drawCreate){
	draw_clear_alpha(c_black, 1);
}

var sX = display_get_gui_width();
var sY = display_get_gui_height();
var cX = sX/2;
var cY = sY/2;

draw_set_colour(c_yellow);
draw_set_alpha(1);

if (!surface_exists(surface)){
	surface = surface_create(surfaceSizeX, surfaceSizeY);
}

if (surface_exists(surface)){
	gpu_set_blendenable(false);
	var pX = cX-surfaceSizeX/2;
	var pY = cY-surfaceSizeY/2;
	draw_surface_ext(surface, pX, pY, 1, 1, 0, c_white, 1);
	gpu_set_blendenable(true);
}
