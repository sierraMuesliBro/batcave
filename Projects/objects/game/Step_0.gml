/// step
#region prepare
var vw = viewIndex;
var cm = view_get_camera(vw);
var vX = camera_get_view_x(cm);
var vY = camera_get_view_y(cm);
var vW = camera_get_view_width(cm);
var vH = camera_get_view_height(cm);
var vCX = vX+vW/2; camera_get_view_width(cm);
var vCY = vY+vH/2; camera_get_view_height(cm);
#endregion

#region camera
if (instance_exists(bat)){
	var cnt = 0;
	var pX = 0;
	var pY = 0;

	with (bat){
		pX += x;
		pY += y;
		cnt += 1;
	}

	if (cnt > 0){
		pX /= cnt;
		pY /= cnt;
	}

	// camera
	if (drawCreate){
		cameraXPre = pX;
		cameraYPre = pY;
		cameraX = pX;
		cameraY = pY;
	}
	else {
		cameraXPre = cameraX;
		cameraYPre = cameraY;
		cameraX = vCX;
		cameraY = vCY;

	    cameraX += (pX-cameraX)*0.25;
	    cameraY += (pY-cameraY)*0.25;

		cameraDirection = point_direction(cameraXPre, cameraYPre, cameraX, cameraY);
	}

	camera_set_view_pos(cm, cameraX-vW/2, cameraY-vH/2);
}


#endregion

#region kill
var ad = angle_difference(cameraDirection, killDirection);
killDirection += ad*0.05;
killDirection = clamp(killDirection, killDirectionMin, killDirectionMax);

if (instance_exists(bat)){
	if (instance_number(bat) > 1){
		var kllDr = killDirection;
		var kllRng = killDirectionRange;
		with (bat){
			var rd = vW/2;
			var pDs = point_distance( x, y, vCX, vCY);
			var pDr = point_direction(x, y, vCX, vCY);
			var rng = angle_difference(pDr, kllDr);

			if (pDs > rd
			and abs(rng) < kllRng){
				instance_destroy();
			}
		}
	}
}
#endregion
