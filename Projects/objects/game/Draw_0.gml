/// draw
if (drawCreate) {
	application_surface_enable(false);

	view_set_surface_id(viewIndex, surface);
	display_set_gui_size(windowSizeX, windowSizeY);
	window_set_size(windowSizeX, windowSizeY);

	view_set_wport(viewIndex   , viewSizeX  );
	view_set_hport(viewIndex   , viewSizeY  );

	draw_clear_alpha(c_black, 1);
}

var vw = viewIndex;
var cm = view_get_camera(vw);
var vX = camera_get_view_x(cm);
var vY = camera_get_view_y(cm);
var vW = camera_get_view_width(cm);
var vH = camera_get_view_height(cm);
var vCX = vX+vW/2;
var vCY = vY+vH/2;

#region view edge surface
if (viewEdgeSurfaceUpdate){
	viewEdgeSurfaceUpdate = false;
	// create if not already
	if (!surface_exists(viewEdgeSurface)){
		viewEdgeSurface = surface_create(viewEdgeSurfaceSizeX, viewEdgeSurfaceSizeY)
	}

	var c1 = c_white;
	var c2 = viewEdgeColour;
	var c3 = c_black;

	surface_set_target(viewEdgeSurface);
		// clear
		gpu_set_blendmode(bm_normal);
		draw_clear_alpha(c1, 1);
		draw_set_circle_precision(64);		

		var iMx = 4;
		var brdRd = 64/iMx;
		for (var i = 0; i <= iMx; ++i) {
			var nrm = i/iMx;
			var brd = i*brdRd;
			var clr = merge_color(c2, c3, lerp(0.5, 1, nrm));
			var alp = lerp(1, 0, nrm);
			draw_set_color(clr);
			draw_set_alpha(1);
			draw_ellipse(0+brd, 0+brd, vW-brd, vH-brd, false);
		}
	surface_reset_target()
	draw_set_alpha(1);
	draw_set_color(c_white);
	gpu_set_blendmode(bm_normal);
}
#endregion

#region kill surface
if (killSurfaceUpdate){
	killSurfaceUpdate = false;
	// create if not already
	if (!surface_exists(killSurface)){
		killSurface = surface_create(killSurfaceSizeX, killSurfaceSizeY)
	}

	var c1 = c_black;
	var c2 = c_red;
	var pX = vW/2;
	var pY = vH/2;
	var brd = 16;

	surface_set_target(killSurface);
	draw_clear_alpha(c_black, 1);
	draw_set_color(c2);
	draw_ellipse(0, 0, vW, vH, false);
	draw_set_colour(c1);
	draw_ellipse(brd, 0, vW-brd, vH, false);
	surface_reset_target();

	if (!sprite_exists(killSurfaceSprite)){
		killSurfaceSprite = sprite_create_from_surface(killSurface, 
														0, 0, 
														killSurfaceSizeX, killSurfaceSizeY, 
														true, true, 
														viewSizeX/2, viewSizeY/2);
	}

	draw_set_alpha(1);
	draw_set_color(c_white);
	gpu_set_blendmode(bm_normal);
}
#endregion

// draw edge
if (surface_exists(viewEdgeSurface)){
	gpu_set_blendmode(bm_subtract);
	draw_surface(viewEdgeSurface, vX, vY);
	gpu_set_blendmode(bm_normal);
}

// draw edge
//if (surface_exists(killSurface)){
//	gpu_set_blendmode(bm_add);
//	draw_surface(killSurface, vX, vY);
//	gpu_set_blendmode(bm_normal);
//}


// draw kill
if (sprite_exists(killSurfaceSprite)){
	gpu_set_blendmode(bm_normal);
	draw_sprite_ext(killSurfaceSprite, 0, vCX, vCY, 1, 1, killDirection, c_white, 0.25);
	gpu_set_blendmode(bm_normal);
}


draw_set_colour(c_white);
draw_set_alpha(1);