{
    "id": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionParent",
    "eventList": [
        {
            "id": "82fc8417-2284-4c92-880e-1c1b56c2e2d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "21febde1-9cf9-49fd-b3b0-d58ab8934a28"
        },
        {
            "id": "3a6c37fe-7ccd-41c7-a970-b950c18e0772",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "21febde1-9cf9-49fd-b3b0-d58ab8934a28"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "6062f87b-8dad-44e0-9fd0-9a0144a3756f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "388a8bba-4679-4356-9376-20cb3e122d42",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "53751709-1482-42e1-a211-570aefa3cb51",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "f9b9735a-ab25-41fa-9b2c-527128c89440",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}