{
    "id": "c95b7da8-1053-4119-a756-faa89b67cc32",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionConcaveTopRightBottomLeft",
    "eventList": [
        {
            "id": "be5b6f51-a1fe-42e6-b127-ce38d5f6db5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c95b7da8-1053-4119-a756-faa89b67cc32"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "b73df068-2264-4c11-a96f-2dc50cfa30a0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 40
        },
        {
            "id": "2f3c50e2-0a63-4012-9e2d-a438a35e118a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 24,
            "y": 48
        },
        {
            "id": "13f0e743-bd7a-4198-b0bb-6d5739e34e40",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 64
        },
        {
            "id": "539f46b4-f091-4db7-adec-1da0ef937445",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        },
        {
            "id": "00813963-58c6-4aca-8c49-087d452f0cb6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}