/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 55CE5A3A
/// @DnDArgument : "code" "/// collision objects$(13_10)tileSetIndexNumber = 30$(13_10)$(13_10)// collision object$(13_10)for (i = 0; i < tileSetIndexNumber; i+=1){$(13_10)	collisionObject[i] = noone$(13_10)}$(13_10)$(13_10)$(13_10)// straight$(13_10)collisionObject[17] = collisionLeft$(13_10)collisionObject[21] = collisionRight$(13_10)collisionObject[10] = collisionTop$(13_10)collisionObject[26] = collisionBottom$(13_10)$(13_10)// convex$(13_10)collisionObject[9] = collisionConvexTopLeft$(13_10)collisionObject[13] = collisionConvexTopRight$(13_10)collisionObject[25] = collisionConvexBottomLeft$(13_10)collisionObject[29] = collisionConvexBottomRight$(13_10)$(13_10)// convex$(13_10)collisionObject[11] = collisionConcaveTopLeft$(13_10)collisionObject[12] = collisionConcaveTopRight$(13_10)collisionObject[27] = collisionConcaveBottomLeft$(13_10)collisionObject[28] = collisionConcaveBottomRight$(13_10)$(13_10)// double$(13_10)collisionObject[1] = collisionConcaveTopLeftBottomRight$(13_10)collisionObject[5] = collisionConcaveTopRightBottomLeft$(13_10)$(13_10)"

{
	/// collision objects
tileSetIndexNumber = 30

// collision object
for (i = 0; i < tileSetIndexNumber; i+=1){
	collisionObject[i] = noone
}


// straight
collisionObject[17] = collisionLeft
collisionObject[21] = collisionRight
collisionObject[10] = collisionTop
collisionObject[26] = collisionBottom

// convex
collisionObject[9] = collisionConvexTopLeft
collisionObject[13] = collisionConvexTopRight
collisionObject[25] = collisionConvexBottomLeft
collisionObject[29] = collisionConvexBottomRight

// convex
collisionObject[11] = collisionConcaveTopLeft
collisionObject[12] = collisionConcaveTopRight
collisionObject[27] = collisionConcaveBottomLeft
collisionObject[28] = collisionConcaveBottomRight

// double
collisionObject[1] = collisionConcaveTopLeftBottomRight
collisionObject[5] = collisionConcaveTopRightBottomLeft


}

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4C293134
/// @DnDArgument : "code" "/// create$(13_10)var lyID = layer_get_id("Tiles_fg");$(13_10)var tmID = layer_tilemap_get_id(lyID);$(13_10)$(13_10)var tlWdth = tilemap_get_tile_width(tmID);$(13_10)var tlHght = tilemap_get_tile_height(tmID);$(13_10)$(13_10)var tmX = tilemap_get_x(tmID);$(13_10)var tmY = tilemap_get_y(tmID);$(13_10)var tmW = tilemap_get_width(tmID);$(13_10)var tmH = tilemap_get_height(tmID);$(13_10)$(13_10)$(13_10)var clLyrID = layer_get_id("Tiles_cl");$(13_10)$(13_10)$(13_10)// check all tiles if not empty create collision object$(13_10)for (iX = 0; iX < tmW; iX+=1){$(13_10)	for (iY = 0; iY < tmH; iY+=1){$(13_10)		var tmDt = tilemap_get(tmID, iX, iY);$(13_10)		var tlIndx = tile_get_index(tmDt);$(13_10)		var tiEmpt = tile_get_empty(tmDt);$(13_10)		$(13_10)$(13_10)		if (!tiEmpt$(13_10)		and collisionObject[tlIndx] != noone){$(13_10)			var pX = tmX + tlWdth*iX;$(13_10)			var pY = tmY + tlHght*iY;$(13_10)			$(13_10)$(13_10)			instance_create_layer(pX, pY, clLyrID, collisionObject[tlIndx])$(13_10)		}$(13_10)	}$(13_10)}"

{
	/// create
var lyID = layer_get_id("Tiles_fg");
var tmID = layer_tilemap_get_id(lyID);

var tlWdth = tilemap_get_tile_width(tmID);
var tlHght = tilemap_get_tile_height(tmID);

var tmX = tilemap_get_x(tmID);
var tmY = tilemap_get_y(tmID);
var tmW = tilemap_get_width(tmID);
var tmH = tilemap_get_height(tmID);


var clLyrID = layer_get_id("Tiles_cl");


// check all tiles if not empty create collision object
for (iX = 0; iX < tmW; iX+=1){
	for (iY = 0; iY < tmH; iY+=1){
		var tmDt = tilemap_get(tmID, iX, iY);
		var tlIndx = tile_get_index(tmDt);
		var tiEmpt = tile_get_empty(tmDt);
		

		if (!tiEmpt
		and collisionObject[tlIndx] != noone){
			var pX = tmX + tlWdth*iX;
			var pY = tmY + tlHght*iY;
			

			instance_create_layer(pX, pY, clLyrID, collisionObject[tlIndx])
		}
	}
}
}

