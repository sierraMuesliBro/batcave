{
    "id": "8b35fc6a-4a41-4cee-aae4-678c0f4e0642",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "tileCollisionGenerator",
    "eventList": [
        {
            "id": "a0c9d045-8dc0-4b99-9dab-05bbe58cb0ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8b35fc6a-4a41-4cee-aae4-678c0f4e0642"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "0cc84695-c036-4236-8377-9ea0336aae7d",
    "visible": true
}