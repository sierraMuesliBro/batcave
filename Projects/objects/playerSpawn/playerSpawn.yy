{
    "id": "01632b57-4446-414c-907d-5d5ebc5b0fcb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "playerSpawn",
    "eventList": [
        {
            "id": "ce90e367-72a5-4ebb-b1a5-9092deb358d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "01632b57-4446-414c-907d-5d5ebc5b0fcb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.1,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4a5324e7-ed24-4ace-87d8-513130c0bf84",
    "visible": true
}