{
    "id": "df001d1d-e0a9-4763-a550-49c869d3d1c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionConvexTopRight",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "e9fd89fb-748d-493c-9287-ddc5e20aacfe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        },
        {
            "id": "becb87d6-ac86-4e24-9e77-81b8bbeadc82",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 40
        },
        {
            "id": "a2268284-93d2-49f7-a777-d1d5018ff6d4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 24,
            "y": 48
        },
        {
            "id": "7981b61c-9920-4621-8641-7a2d3668d9fe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 64
        },
        {
            "id": "be536f4c-d758-4afc-b745-895bf75ed1ab",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}