{
    "id": "df03369e-9659-4c4b-9498-10518184d59f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionConcaveBottomRight",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "0bca9b02-8920-4e0c-ba45-5452f610fab2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "49b9302b-821d-49fe-96fc-b63ea747d16b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "89e9d755-f6fc-4e6e-93bf-54a18d135541",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 32
        },
        {
            "id": "dc036622-a492-4474-802e-e177e109fd97",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 64
        },
        {
            "id": "3152b1fa-bfcd-4660-a64e-a58033c7c2e3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}