{
    "id": "bb4c00d6-168e-4abb-ae2d-43dc8b366b83",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionTop",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "cf1cf86c-a895-4330-b33e-4a370ff1ff0a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        },
        {
            "id": "c3d07ad9-f397-45ca-ba1a-5f91360673eb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 32
        },
        {
            "id": "830b0bdd-1777-466f-85bd-1814596376c0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "03b5306f-a7e3-482c-8995-f19236abc197",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}