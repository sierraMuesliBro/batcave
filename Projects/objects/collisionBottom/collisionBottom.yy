{
    "id": "edf55beb-d0ae-49bd-a846-38b26290435d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionBottom",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "4c139d83-cb00-4d8f-8bb9-0995e4470ead",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "fcaa0fee-4cdd-4990-b819-4193a688afb3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "f240384d-491a-49a0-854b-93f1975696b4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 32
        },
        {
            "id": "859cbd50-cd43-4b3e-a839-57331d0b6d2e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}