/// @desct stFall
var stPre = statePrevious_sc()
if (stPre != noone){
	// sprite
	sprite_index = spFall;
	image_index = 3;
	image_speed = 1;
	
	if (stPre = stDive) {
	
	}


	// glide
	glideAmount = 0.25;

	
	// input
	inHold = false;
	inTap  = false;
}

// flap
if (inHold){
	stateChange_sc(stFlap);
	exit;
}

// loop frames
if (imageIndexEnd_sc()){
	image_index = 5;
} 


/// angle
angleTarget = 90-90*sign(image_yscale)
if (phy_rotation != angleTarget){
	var anglDffrnc = angle_difference(angleTarget, phy_rotation);

	if (abs(anglDffrnc) > anglePlus){
		phy_rotation += anglePlus*sign(anglDffrnc);
		phy_angular_velocity += anglePlus*sign(anglDffrnc);
	}
	else phy_rotation = angleTarget;
}


objectApplyGlideForce_sc();
//physicsGlideForce_sc(0.5);

