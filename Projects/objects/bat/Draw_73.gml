/// draw info


/// water turn force
/*
var pntDir =  point_direction(0, 0, phy_speed_x, phy_speed_y);
var angDif =  angle_difference(-phy_rotation, pntDir);
if (abs(angDif) >= 90) angDif = (90-(abs(angDif)-90))*sign(angDif)
var maDir = (pntDir - 90 - angDif);
var maSpd = abs(dsin(angDif))*phy_speed*0.25;

var fX = dcos(maDir)*maSpd;
var fY = -dsin(maDir)*maSpd;

physics_apply_force(x, y, fX, fY)
//phy_speed_x += dcos(maDir)*maSpd
//phy_speed_y -= dsin(maDir)*maSpd
*/

/*
var dir =  point_direction(0, 0, phy_speed_x, phy_speed_y);

var ang = -phy_rotation+360+90;

var angDif = angle_difference(ang, dir);
if (abs(angDif) >= 90) angDif = (90-(abs(angDif)-90))*sign(angDif)

var fDir = dir-angDif;
var fSpd = abs(dcos(angDif))*phy_speed*0.5;

var fX = -dcos(fDir)*fSpd;
var fY = dsin(fDir)*fSpd;

physics_apply_force(x, y, fX, fY)

var dX = dcos(dir)*64;
var dY = -dsin(dir)*64;

var aX = dcos(ang)*32;
var aY = -dsin(ang)*32;

var adX = fX*(8+32*fSpd);
var adY = fY*(8+32*fSpd);

draw_set_alpha(1)
draw_set_color(c_yellow)
draw_arrow(x, y, x+dX, y+dY, 4) // direction
draw_set_color(c_aqua)
draw_arrow(x, y, x+aX, y+aY, 4) // angle
draw_set_color(c_fuchsia)
draw_arrow(x, y, x+adX, y+adY, 4) // angle
draw_text(x,y,string(phy_speed))