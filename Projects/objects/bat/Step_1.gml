/// input
var prs = false; // press
var hld = false; // hold
var rls = false; // release

/// keyboard
var ky = playerInputKey[playerIndex];
if (keyboard_check_pressed(ky))  prs = true
if (keyboard_check(ky))          hld = true
if (keyboard_check_released(ky)) rls = true


/// input

// tap
if (prs) {
	inTapTimer = inTapTime
	inTap = true;
}
else inTap = false;

// hold
if (hld) {
	inHold = true;
}
else {
	inHold = false;
}

// release
if (rls) {
	 if (inTapTimer > -1){
		inTap = true;
		inTapTimer = inTapTime;
	 }
}
else rls = false

// timer
if (inTapTimer > -1){
	inTapTimer -= 1;

	if (inTapTimer = 0){
		inTap = false;
	}
	else if (inTapTimer < 0) inTapTimer = -1;
}