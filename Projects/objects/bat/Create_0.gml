/// create
phy_fixed_rotation = true;
phy_rotation = 90;

// flap
flapPlus = 3;
flapSpeedMax = 10;

// angle
angleTarget = 0;
anglePlus = 3;
angleFlapPlus = 2;
angleSnap = 1;

// glide
glideAmount = 0.25;

/// States
stHang = 0;
stDrop = 1;
stFall = 2;
stFlap = 3;
stDive = 4;
stLand = 5;
stIdle = 6;

state = stHang;
statePre = noone;

/// sprites
spHang = batHang_sp;
spDrop = batDrop_sp;
spFall = batFall_sp;
spFlap = batFlap_sp;
spDive = batDive_sp;


// image
previousNumber = 10
for (i=0; i<previousNumber; i+=1){
	imageIndexPre[i] = image_index;
}

imageIndexPre = image_index;

/// input
inTap = false;
inHold = false;
inDive = false;
inFlap = false;

inTapTime = 5;
inTapTimer = -1;

/// player
playerIndex = 0;
playerNumberMax = 4;
playerColour[0] = c_white;
playerColour[1] = c_red;
playerColour[2] = c_green;
playerColour[3] = c_fuchsia;

playerInputKey[0] = ord("A");
playerInputKey[1] = ord("F");
playerInputKey[2] = ord("J");
playerInputKey[3] = 186;