/// @desct stDrop
var stPre = statePrevious_sc()
if (stPre != noone){
	// sprite
	sprite_index = spDrop;
	image_index = 0;
}


/// angle
angleTarget = 90-90*sign(image_yscale)
if (inHold) angleTarget = 90
if (phy_rotation != angleTarget){
	var anglDffrnc = angle_difference(angleTarget, phy_rotation);

	if (abs(anglDffrnc) > anglePlus){
		phy_rotation += anglePlus*sign(anglDffrnc);
		phy_angular_velocity += anglePlus*sign(anglDffrnc);
	}
	else phy_rotation = angleTarget;
}


/// to Fall
if (imageIndexEnd_sc()){
	if (inHold) stateChange_sc(stDive);
	else		stateChange_sc(stFall);
}