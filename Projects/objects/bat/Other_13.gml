/// @desct stFlap
var stPre = statePrevious_sc()
if (stPre != noone){
	// sprite
	sprite_index = spFlap
	image_index = 0
	image_speed = 1

	// input
	inDive = true
	inFlap = false
	
	// glide
	glideAmount = 0.25
}


// input
if (inDive
and inHold = false) inDive = false
else if (inTap) inFlap = true



// to dive
if (inDive
and imageIndexPass_sc(4)){
	stateChange_sc(stDive)
	exit
}


// to flap
if (inFlap
and imageIndexEnd_sc()){
	stateReset_sc()
	exit
}

// to Fall
if (imageIndexEnd_sc()){
	stateChange_sc(stFall)
	exit
}



// push up
if (imageIndexPass_sc(4)){
	var spY = clamp(flapSpeedMax-(-phy_speed_y), 0, flapPlus);
	phy_speed_y -= spY
}




// flap angle
//if (image_index >= 2){
	angleTarget = -90+20*sign(image_yscale)
	if (phy_rotation != angleTarget){
		var anglDffrnc = angle_difference(angleTarget, phy_rotation)

		if (abs(anglDffrnc) > angleFlapPlus){
			 phy_rotation += angleFlapPlus*sign(anglDffrnc)
		}
		else phy_rotation = angleTarget
	}
//}


/*
// image speed
switch (floor(image_index)){
    case 0: image_speed = 0.5 break;
    default: image_speed = 1
}
*/


// glide force
// glide force
if (phy_speed_y > 0){
	objectApplyGlideForce_sc();
}

