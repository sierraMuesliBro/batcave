/// @desct stDive
var stPre = statePrevious_sc()
if (stPre != noone){

	if (sign(dcos(phy_rotation)) = -sign(image_yscale)){
		//image_yscale = -image_yscale
		
	}

	// sprite
	sprite_index = spDive
	image_index = 0
	

	switch (stPre){
	    case stDrop:
			//image_index = 4
		break
	    default:
	}
	

	// glide
	glideAmount = 0.25
}

// to fall
if (image_index > 3
and inHold = false){
	stateChange_sc(stFall)
}

// push
if (image_index == 1){
	var spd = flapPlus;//clamp(flapSpeedMax-(-phy_speed_y), 0, flapPlus)*sign(image_xscale);
	var ang = (-phy_rotation+45)*sign(image_yscale)
	//var angSgn = sign(dcos(phy_rotation));

	phy_speed_x += dcos(ang)*spd
	phy_speed_y -= dsin(ang)*spd
}


// loop frames
if (imageIndexEnd_sc()){
	image_index = 5;
}



//phy_rotation += anglePlus*sign(image_yscale)

// angle
angleTarget = 90// + 10*sign(image_xscale)
if (phy_rotation != angleTarget){
	var anglDffrnc = angle_difference(angleTarget, phy_rotation)

	if (abs(anglDffrnc) > anglePlus){
		 phy_rotation += anglePlus*sign(anglDffrnc)
	}
	else phy_rotation = angleTarget
}



// glide force
objectApplyGlideForce_sc();

