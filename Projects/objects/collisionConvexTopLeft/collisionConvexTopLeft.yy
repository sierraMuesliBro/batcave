{
    "id": "45c909f3-7f36-4738-b27b-34a4ca0f2ded",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionConvexTopLeft",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "a90ab58b-3064-48f1-a004-7db5c8e83a3f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 64
        },
        {
            "id": "cd17b3cc-3323-412b-b02b-f54ca3c2b508",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 40,
            "y": 48
        },
        {
            "id": "095a0155-7801-4916-b74f-60ff72715d51",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 40
        },
        {
            "id": "694fcdb0-bb08-4303-9546-d56e677dd938",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 32
        },
        {
            "id": "c42f8eb5-8a70-421f-b1d3-2e2af015b7fc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}