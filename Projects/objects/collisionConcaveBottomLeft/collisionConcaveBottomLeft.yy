{
    "id": "2af0a4ca-0431-4994-8e06-e6e3ab82466a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionConcaveBottomLeft",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "03c2ddc7-76ce-4d31-b48d-8884e82864a7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        },
        {
            "id": "aecdd134-96ca-4c18-b9c3-ead068799a48",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "e8a7a377-a586-4c57-889e-d3063458607e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "6fbb0ee6-50df-4145-8e7a-25f92d278ddd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "4b91707b-d848-427e-bee5-2f846e06f3a7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 64
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}