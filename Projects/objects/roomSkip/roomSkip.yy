{
    "id": "6cea7f95-362b-4a5b-b834-ecd14be8a0be",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "roomSkip",
    "eventList": [
        {
            "id": "5337d724-c6fc-457d-b35f-bc4da38f88dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6cea7f95-362b-4a5b-b834-ecd14be8a0be"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "168b4033-c28d-4b06-b189-17b21b54ad33",
    "visible": true
}