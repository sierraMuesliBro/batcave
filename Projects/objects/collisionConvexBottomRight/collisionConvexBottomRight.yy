{
    "id": "3a919c3c-a08f-4c2d-aa8f-7a3db1fa078b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionConvexBottomRight",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "47c6ea6f-2b0a-4e09-b8f7-98a37988f0f6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "862aecbf-83f1-42d6-96e7-5d3e9aaeeab8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "b5c68932-7de9-4c73-9103-49306e7513d2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 24,
            "y": 16
        },
        {
            "id": "1512c48d-cd4d-452f-8081-71740fc6801e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 24
        },
        {
            "id": "e028b6b6-706c-41fc-b65e-d35f72d951f3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}