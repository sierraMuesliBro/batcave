{
    "id": "b8b0c6bd-a87a-4f06-aa4f-0b929176eae8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collisionConcaveTopLeftBottomRight",
    "eventList": [
        {
            "id": "1d26a6c6-0769-4ae3-9ad8-7e4d3c1582df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8b0c6bd-a87a-4f06-aa4f-0b929176eae8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "21febde1-9cf9-49fd-b3b0-d58ab8934a28",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "358bc72d-24b5-40bc-808f-48ad53e3bbee",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 64
        },
        {
            "id": "4211d81f-da6c-4f14-ab6d-bf6cfb16def4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 40,
            "y": 48
        },
        {
            "id": "4c7837a0-2ac3-4a2b-819b-5cd08d9a68ae",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 40
        },
        {
            "id": "8bfd8952-3f29-4889-8ce3-f00bf30ffffe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 32
        },
        {
            "id": "4ccd72dd-dd01-4bc7-b544-8c9472dd28fc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        }
    ],
    "physicsStartAwake": false,
    "solid": false,
    "spriteId": "78d192d6-512e-405d-8f45-d4c1958a5af9",
    "visible": true
}