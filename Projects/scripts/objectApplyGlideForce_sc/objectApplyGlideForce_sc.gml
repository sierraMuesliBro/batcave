/// @description  objectApplyGlideForce_sc(amount, angle, speed, direction)
/// @param amount
/// @param angle
/// @param speed
/// @param direction
/*
    this fuction applies force to the object like it has wings or like floating leaf
*/

var amt = 0.5;            // how much force to apply
var ang = -phy_rotation+360;    // the angle of the wings of the gliding object
var spd = phy_speed;       // how fast the object is going
var dr  = point_direction(0, 0, phy_speed_x, phy_speed_y);     // the direction the object is moving in

// take arguments
//switch (argument_count){
//    case 4: ang = argument[3]; // angle
//    case 3: dr = argument[2]; // direction
//    case 2: spd = argument[1]; // speed
//    case 1: amt = argument[0]; // amount
//    break;
//    default:
//	break;
//}

var angDf = angle_difference(dr, ang);
//if (abs(angDf) >= 90) angDf = (90-(abs(angDf)-90))*sign(angDf)

var frcDr = ang+90*sign(angDf);//dr-angDf;
var frcSpd = abs(dsin(angDf))*spd*amt;

var fX = dcos(frcDr)*frcSpd;
var fY = -dsin(frcDr)*frcSpd;

physics_apply_force(x, y, -fX, -fY)

//phy_speed_x -= fX;
//phy_speed_y -= fY;

/*
/// debug draw
var dX = dcos(dr)*64;
var dY = -dsin(dr)*64;

var aX = dcos(ang)*32;
var aY = -dsin(ang)*32;

//var fdX = fX*(8+16*frcSpd);
//var fdY = fY*(8+16*frcSpd);

var fdX = dcos(frcDr)*48;
var fdY = -dsin(frcDr)*48;

draw_set_alpha(1)
draw_set_color(c_yellow)
draw_arrow(x, y, x+dX, y+dY, 4) // direction
draw_set_color(c_aqua)
draw_arrow(x, y, x+aX, y+aY, 4) // angle
draw_set_color(c_fuchsia)
draw_arrow(x, y, x+fdX, y+fdY, 4) // angle
