/// imageIndexRange_sc()
var val = 0;
var val2 = image_number;

if (argument_count > 0) {
	val = argument[0]
}

if (argument_count > 1) {
	val2 = argument[1]
}

if (image_index >= val
and image_index <  val2) return true
