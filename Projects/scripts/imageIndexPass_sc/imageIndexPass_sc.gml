/// imageIndexPass_sc()
var val = 0;

if (argument_count > 0) val = argument[0]


if (image_speed > 0){
	if (image_index >= val
	and imageIndexPre[1] < val) return true
}
else if (image_speed < 0){
	if (imageIndexPre[1] >= val
	and image_index < val) return true
}
else {
	if (image_index >= 0) return true
}