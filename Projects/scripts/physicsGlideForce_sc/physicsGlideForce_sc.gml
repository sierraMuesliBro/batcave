/// physicsGlideForce_sc()
var amnt = 0.1
if (argument_count > 0) amnt = argument[0]


var dir =  point_direction(0, 0, phy_speed_x, phy_speed_y);
var ang = -phy_rotation+360;

var angDif = angle_difference(dir, ang);
//if (abs(angDif) >= 90) angDif = (90-(abs(angDif)-90))*sign(angDif)

var frcDir = ang+90*sign(angDif)//dir-angDif;
var frcSpd = abs(dsin(angDif))*phy_speed*amnt;

var fX = dcos(frcDir)*frcSpd;
var fY = -dsin(frcDir)*frcSpd;

physics_apply_force(x, y, -fX, -fY)


/// debug draw

var dX = dcos(dir)*64;
var dY = -dsin(dir)*64;

var aX = dcos(ang)*32;
var aY = -dsin(ang)*32;

var fdX = fX*(8+16*frcSpd);
var fdY = fY*(8+16*frcSpd);

//var fdX = dcos(frcDir)*48;
//var fdY = -dsin(frcDir)*48;

draw_set_alpha(1)
draw_set_color(c_yellow)
draw_arrow(x, y, x+dX, y+dY, 4) // direction
draw_set_color(c_aqua)
draw_arrow(x, y, x+aX, y+aY, 4) // angle
draw_set_color(c_fuchsia)
draw_arrow(x, y, x+fdX, y+fdY, 4) // angle
draw_text(x,y,string(phy_speed))