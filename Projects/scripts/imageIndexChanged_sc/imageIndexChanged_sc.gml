/// imageIndexChanged_sc(to, from)
/*
	():         if index not previous
	(to):       if index has changed to
	(to, from): if index change to and came from
	(-1, from):	if index has changed from
*/

var to = -1;
var frm = -1;

if (argument_count > 0){
	if (argument[0] != -1) to = argument[0]
}
if (argument_count > 1){
	if (argument[1] != -1) frm = argument[1]
}

 // (to, from)
if (to != -1
and frm != -1){
	if (to = image_index
	and frm = imageIndexPre[1]) return true
} // (to)
else if (to != -1){
	if (to != imageIndexPre[1]) return true
} // (-1, from)
else if (frm != -1){
	if (frm = imageIndexPre[1]) return true
} // ()
else{
	if (image_index != imageIndexPre[1]) return true
}